import { IsString, IsNotEmpty } from 'class-validator';
import { Expose } from 'class-transformer';

export class SendCodeDto {
    @IsString()
    @IsNotEmpty()
    @Expose()
    message: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    fromNumber: string;
}
