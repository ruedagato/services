import { IsString, IsNotEmpty, IsNumber } from 'class-validator';
import { Expose } from 'class-transformer';

export class ValidateCodeDto {
    @IsNumber()
    @IsNotEmpty()
    @Expose()
    code: number;

    @IsString()
    @IsNotEmpty()
    @Expose()
    fromNumber: string;
}