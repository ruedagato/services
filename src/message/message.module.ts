import { Module } from '@nestjs/common';
import { SmsController } from './controllers/sms.controller';
import { TwilioService } from './services/twilio.service';
import { MessageService } from './services/message.service';

@Module({
  controllers: [SmsController],
  providers: [TwilioService, MessageService]
})
export class MessageModule {}
