import { Injectable, BadRequestException } from '@nestjs/common';
import { database } from 'firebase-admin';
import { plainToClass } from 'class-transformer';
import { TwilioKeys } from '../models/twilioKeys';
import * as twilio from 'twilio';

@Injectable()
export class TwilioService {
    readonly twilioRef = database().ref('twilio');

    saveCredentials(twilioKeys: TwilioKeys, uid: string) {
        return this.twilioRef.child(uid).set(twilioKeys.serialize());
    }

    async getCredentials(uid: string) {
        const snap = await this.twilioRef.child(uid).once('value');
        if (!snap.val()) {
            throw new BadRequestException('not set twilio keys');
        }
        return plainToClass(TwilioKeys, snap.val());
    }

    sendMessage(to: string, message: string, twilioKeys: TwilioKeys) {
        const client = twilio(twilioKeys.accountSid, twilioKeys.authToken);
        return client.messages.create({
            body: message,
            from: twilioKeys.fromNumber,
            to,
        });
    }
}
