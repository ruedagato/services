import { Injectable } from '@nestjs/common';
import { TwilioService } from './twilio.service';
import { database } from 'firebase-admin';

@Injectable()
export class MessageService {
    // ========================================================================
    // Variables
    // ========================================================================
    private readonly refCode = database().ref('codes');

    // ========================================================================
    // Constructor
    // ========================================================================
    constructor(private twilio: TwilioService) {}

    async senCodeSms(uid: string, templateMessage: string, num: string) {
        const { code, message } = this.generateCode(templateMessage);
        const tokens = await this.twilio.getCredentials(uid);
        const respond = await Promise.all([
            this.twilio.sendMessage(num, message, tokens),
            this.saveCodeValidator(code, uid, num),
        ]);
        return respond[0];
    }

    async validateCode(uid: string, num: string, code: number) {
        const snap = await this.refCode
            .child(uid)
            .child(num)
            .once('value');
        if (Number(snap.val()) === Number(code)) {
            this.refCode
                .child(uid)
                .child(num)
                .remove();
            return { valid: true };
        }
        return { valid: false };
    }

    private generateCode(templateMessage: string) {
        const code = Math.trunc(Math.random() * 100000);
        const message = templateMessage.replace('__CODE__', code + '');
        return { code, message };
    }

    private saveCodeValidator(code: number, uid: string, num: string) {
        return this.refCode
            .child(uid)
            .child(num)
            .set(code);
    }
}
