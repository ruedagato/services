import { Controller, Get, UseGuards, Post, Body } from '@nestjs/common';
import { AuthGuard } from '../../guards/auth.guard';
import { Company } from '../../decorator/company.decorator';
import { TwilioService } from '../services/twilio.service';
import { TwilioKeys } from '../models/twilioKeys';
import { plainToClass } from 'class-transformer';
import { SendCodeDto } from '../dto/sendCode.dto';
import { MessageService } from '../services/message.service';
import { ValidateCodeDto } from '../dto/validateCode.dto';

@Controller('sms')
export class SmsController {

    constructor(private twilio: TwilioService, private message: MessageService) {}

    @Get()
    @UseGuards(AuthGuard)
    tempo(@Company() data: any) {
        const {uid} = data;
        return {uid};
    }

    @Post('set_keys')
    @UseGuards(AuthGuard)
    saveKeys(@Company() data: any, @Body() twilioKeys: TwilioKeys) {
        const {uid} = data;
        return this.twilio.saveCredentials(plainToClass(TwilioKeys, twilioKeys), uid);
    }

    @Post('send_code')
    @UseGuards(AuthGuard)
    async sendMessage(@Company() data: any, @Body() req: SendCodeDto) {
        const {uid} = data;
        return this.message.senCodeSms(uid, req.message, req.fromNumber);
    }

    @Post('validate_code')
    @UseGuards(AuthGuard)
    async validateCode(@Company() data: any, @Body() req: ValidateCodeDto) {
        const {uid} = data;
        return this.message.validateCode(uid, req.fromNumber, req.code);
    }

}
