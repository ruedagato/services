import { IsNotEmpty, IsString } from 'class-validator';
import { Expose } from 'class-transformer';
import { Base } from '../../utils/base';

export class TwilioKeys extends Base {
    @IsString()
    @IsNotEmpty()
    @Expose()
    accountSid: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    authToken: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    fromNumber: string;
}
