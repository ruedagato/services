import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import * as path from 'path';
import * as dotenv from 'dotenv';
import * as admin from 'firebase-admin';
import serviceAccount = require('./admin.json');
import { ValidationPipe } from '@nestjs/common';

dotenv.config({
    path: path.resolve(__dirname, '../.env'),
});

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount as any),
    databaseURL: process.env.FIREBASE,
});

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.useGlobalPipes(new ValidationPipe());
    await app.listen(3000);
}
bootstrap();
