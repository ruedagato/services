import { IsString, IsNotEmpty } from 'class-validator';
import { Expose } from 'class-transformer';

export class CreateSubscription {
    @IsString()
    @IsNotEmpty()
    @Expose()
    idUser: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    tokenCard: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    planCode: string;
}
