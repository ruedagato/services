import { IsString, IsNotEmpty, IsNumber } from 'class-validator';
import { Expose } from 'class-transformer';
import { CardDto } from './card.dto';

export class CardClientDto extends CardDto {
    @IsString()
    @IsNotEmpty()
    @Expose()
    line1: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    city: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    state: string;
}
