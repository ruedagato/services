import { IsString, IsNotEmpty } from 'class-validator';
import { Expose } from 'class-transformer';

// Generated by https://quicktype.io

export class PayTokenDto {
    @IsString()
    @IsNotEmpty()
    @Expose()
    token: string;
}

export interface PaymentTokenDto {
    language: string;
    command: string;
    merchant: Merchant;
    transaction: Transaction;
    test: boolean;
}

export interface Merchant {
    apiKey: string;
    apiLogin: string;
}

export interface Transaction {
    order: Order;
    payer: Payer;
    creditCardTokenId: string;
    extraParameters: ExtraParameters;
    type: string;
    paymentMethod: string;
    paymentCountry: string;
    deviceSessionId: string;
    ipAddress: string;
    cookie: string;
    userAgent: string;
}

export interface ExtraParameters {
    INSTALLMENTS_NUMBER: number;
}

export interface Order {
    accountId: string;
    referenceCode: string;
    description: string;
    language: string;
    signature: string;
    notifyUrl: string;
    additionalValues: AdditionalValues;
    buyer: Buyer;
    shippingAddress: IngAddress;
}

export interface AdditionalValues {
    TX_VALUE: TxValue;
}

export interface TxValue {
    value: number;
    currency: string;
}

export interface Buyer {
    merchantBuyerId: string;
    fullName: string;
    emailAddress: string;
    contactPhone: string;
    dniNumber: string;
    shippingAddress: IngAddress;
}

export interface IngAddress {
    street1: string;
    street2: string;
    city: string;
    state: string;
    country: string;
    postalCode: string;
    phone: string;
}

export interface Payer {
    merchantPayerId: string;
    fullName: string;
    emailAddress: string;
    contactPhone: string;
    dniNumber: string;
    billingAddress: IngAddress;
}
