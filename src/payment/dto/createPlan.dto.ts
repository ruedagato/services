import { IsString, IsNotEmpty } from 'class-validator';
import { Expose } from 'class-transformer';

export class CreatePlanDto {
    @IsString()
    @IsNotEmpty()
    @Expose()
    planCode: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    description: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    interval: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    value: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    currency: string;
}
