import { IsString, IsNotEmpty, IsNumber } from 'class-validator';
import { Expose } from 'class-transformer';

export class CardDto {
    @IsString()
    @IsNotEmpty()
    @Expose()
    payerId: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    name: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    identificationNumber: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    paymentMethod: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    number: string;

    @IsNumber()
    @IsNotEmpty()
    @Expose()
    expirationYear: number;

    @IsNumber()
    @IsNotEmpty()
    @Expose()
    expirationDay: number;
}
