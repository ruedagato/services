import { IsString, IsNotEmpty } from 'class-validator';
import { Expose } from 'class-transformer';
import { Base } from '../../utils/base';

export class NewClientDto extends Base {
    @IsString()
    @IsNotEmpty()
    @Expose()
    fullName: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    email: string;
}
