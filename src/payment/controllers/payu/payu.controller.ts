import { Controller, Get, Post, UseGuards, Body } from '@nestjs/common';
import { PayuService } from '../../services/payu/payu.service';
import { AuthGuard } from '../../../guards/auth.guard';
import { Company } from '../../../decorator/company.decorator';
import { PayuKeys } from '../../models/payuKeys';
import { plainToClass } from 'class-transformer';
import { CardDto } from '../../dto/card.dto';
import { PayTokenDto } from '../../dto/payToken.dto';
import { PayuPlanService } from '../../services/payu-plan/payu-plan.service';
import { CreatePlanDto } from '../../dto/createPlan.dto';
import { NewClientDto } from '../../dto/newClient.dto';
import { PayuClientService } from '../../services/payu-client/payu-client.service';
import { PayuSubscriptionService } from '../../services/payu-subscription/payu-subscription.service';
import { CardClientDto } from '../../dto/cardclient.dto';
import { CreateSubscription } from '../../dto/createSubscription.dto';

@Controller('payu')
@UseGuards(AuthGuard)
export class PayuController {
    constructor(
        private payuService: PayuService,
        private payuPlans: PayuPlanService,
        private payuClient: PayuClientService,
        private payuSubscription: PayuSubscriptionService,
    ) {}

    @Post('keys')
    saveKeys(@Company() data: any, @Body() payuKeys: PayuKeys) {
        const { uid } = data;
        return this.payuService.saveCredentials(plainToClass(PayuKeys, payuKeys), uid);
    }

    @Post('token')
    tokenCard(@Company() data: any, @Body() cardDto: CardDto) {
        const { uid } = data;
        return this.payuService.tokenCard(cardDto, uid);
    }

    @Post('payToken')
    payToken(@Company() data: any, @Body() payTokenDto: PayTokenDto) {
        const { uid } = data;
        return this.payuService.payWithToken(uid, payTokenDto);
    }

    @Post('plan')
    createPlan(@Company() data: any, @Body() planDto: CreatePlanDto) {
        const { uid } = data;
        return this.payuPlans.create(uid, planDto);
    }

    @Post('client')
    createClient(@Company() data: any, @Body() newClient: NewClientDto) {
        const { uid } = data;
        return this.payuClient.create(uid, plainToClass(NewClientDto, newClient));
    }

    @Post('client/card')
    createCardClient(@Company() data: any, @Body() card: CardClientDto) {
        const { uid } = data;
        return this.payuClient.addCard(uid, card);
    }

    @Post('subscription')
    createSubscription(@Company() data: any, @Body() subscription: CreateSubscription) {
        const { uid } = data;
        return this.payuSubscription.create(uid, subscription);
    }
}
