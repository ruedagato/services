import { Test, TestingModule } from '@nestjs/testing';
import { PayuController } from './payu.controller';

describe('Payu Controller', () => {
  let controller: PayuController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PayuController],
    }).compile();

    controller = module.get<PayuController>(PayuController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
