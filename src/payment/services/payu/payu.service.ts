import { Injectable, HttpService } from '@nestjs/common';
import { RespondToken } from '../../models/respondToken';
import { map } from 'rxjs/operators';
import { database } from 'firebase-admin';
import { PayuKeys } from '../../models/payuKeys';
import { CardDto } from '../../dto/card.dto';
import { PayTokenDto } from '../../dto/payToken.dto';

@Injectable()
export class PayuService {
    // ========================================================================
    // Variables
    // ========================================================================

    private readonly payuUrl = process.env.PAYU + '/4.0/service.cgi';

    private readonly payuRef = database().ref('payu');

    // ========================================================================
    // constructor
    // ========================================================================

    constructor(private http: HttpService) {}

    // ========================================================================
    // Methods
    // ========================================================================

    /**
     * Save keys payu
     * @param payuKeys payu keys
     * @param uid uid company
     */
    saveCredentials(payuKeys: PayuKeys, uid: string) {
        return this.payuRef.child(uid).set(payuKeys.serialize());
    }

    /**
     * tokenization card
     * @param cardToken Card info to tokenization
     * @param uid uid company
     */
    async tokenCard(cardToken: CardDto, uid: string) {
        const keys = await this.getKeys(uid);
        const dayString = cardToken.expirationDay > 9 ? cardToken.expirationDay : '0' + cardToken.expirationDay;
        const expiration = cardToken.expirationYear + '/' + dayString;
        return this.http
            .post<RespondToken>(this.payuUrl, {
                language: 'es',
                command: 'CREATE_TOKEN',
                merchant: {
                    apiLogin: keys.apiLogin,
                    apiKey: keys.apiKey,
                },
                creditCardToken: {
                    payerId: cardToken.payerId,
                    name: cardToken.name,
                    identificationNumber: cardToken.identificationNumber,
                    paymentMethod: cardToken.paymentMethod,
                    number: cardToken.number,
                    expirationDate: expiration,
                },
            })
            .pipe(map(axData => axData.data));
    }

    async payWithToken(uid: string, payTokenDto: PayTokenDto) {
        const { apiKey, apiLogin, accountId, currency, country } = await this.getKeys(uid);
        const data = {
            language: 'es',
            command: 'SUBMIT_TRANSACTION',
            merchant: {
                apiKey,
                apiLogin,
            },
            transaction: {
                order: {
                    accountId,
                    referenceCode: 'payment_test_00000001',
                    description: 'payment test',
                    language: 'es',
                    signature: '971dd1f8bd4c7b43eae2233464d4c97e',
                    notifyUrl: 'http://www.tes.com/confirmation',
                    additionalValues: {
                        TX_VALUE: {
                            value: 10000,
                            currency,
                        },
                    },
                    buyer: {
                        merchantBuyerId: '1',
                        fullName: 'APPROVED',
                        emailAddress: 'buyer_test@test.com',
                        contactPhone: '7563126',
                        dniNumber: '5415668464654',
                        shippingAddress: {
                            street1: 'calle 100',
                            street2: '5555487',
                            city: 'Medellin',
                            state: 'Antioquia',
                            country: 'CO',
                            postalCode: '000000',
                            phone: '7563126',
                        },
                    },
                    shippingAddress: {
                        street1: 'calle 100',
                        street2: '5555487',
                        city: 'Medellin',
                        state: 'Antioquia',
                        country: 'CO',
                        postalCode: '0000000',
                        phone: '7563126',
                    },
                },
                payer: {
                    merchantPayerId: '1',
                    fullName: 'First name and second payer name',
                    emailAddress: 'payer_test@test.com',
                    contactPhone: '7563126',
                    dniNumber: '5415668464654',
                    billingAddress: {
                        street1: 'calle 93',
                        street2: '125544',
                        city: 'Bogota',
                        state: 'Bogota DC',
                        country: 'CO',
                        postalCode: '000000',
                        phone: '7563126',
                    },
                },
                creditCardTokenId: payTokenDto.token,
                extraParameters: {
                    INSTALLMENTS_NUMBER: 1,
                },
                type: 'AUTHORIZATION_AND_CAPTURE',
                paymentMethod: 'VISA',
                paymentCountry: country,
                deviceSessionId: 'vghs6tvkcle931686k1900o6e1',
                ipAddress: '127.0.0.1',
                cookie: 'pt1t38347bs6jc9ruv2ecpv7o2',
                userAgent: 'Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0',
            },
            test: process.env.ENV === 'dev',
        };
        return this.http.post(this.payuUrl, data).pipe(map(axData => axData.data));
    }

    // ========================================================================
    // Private methods
    // ========================================================================
    private async getKeys(uid: string) {
        const snap = await this.payuRef.child(uid).once('value');
        return snap.val() as PayuKeys;
    }
}
