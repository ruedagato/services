import { Injectable, BadRequestException } from '@nestjs/common';
import * as request from 'request-promise';
import { NewClientDto } from '../../dto/newClient.dto';
import { database } from 'firebase-admin';
import { PayuKeys } from '../../models/payuKeys';
import { CardClientDto } from '../../dto/cardclient.dto';

@Injectable()
export class PayuClientService {
    // ========================================================================
    // Variables
    // ========================================================================
    private readonly url = process.env.PAYU + '/rest/v4.9/customers';

    private payuRef = database().ref('payu');

    // ========================================================================
    // Methods
    // ========================================================================

    async create(uid: string, newClient: NewClientDto) {
        const keys = await this.getKeys(uid);
        const options = {
            method: 'POST',
            url: this.url,
            body: newClient.serialize(),
            json: true,
        };
        return this.sendRequest(options, keys);
    }

    async addCard(uid: string, card: CardClientDto) {
        const keys = await this.getKeys(uid);
        const data = {
            name: card.name,
            document: card.identificationNumber,
            number: card.number,
            expMonth: card.expirationDay > 9 ? card.expirationDay + '' : '0' + card.expirationDay,
            expYear: card.expirationYear + '',
            type: card.paymentMethod,
            address: {
                line1: card.line1,
                city: card.city,
                state: card.state,
                country: keys.country,
            },
        };
        const options = {
            method: 'POST',
            url: `${this.url}/${card.payerId}/creditCards`,
            body: data,
            json: true,
        };
        return this.sendRequest(options, keys);
    }

    // ========================================================================
    // Private methods
    // ========================================================================
    async getKeys(uid: string) {
        const snap = await this.payuRef.child(uid).once('value');
        return snap.val() as PayuKeys;
    }

    async sendRequest(options: any, keys: PayuKeys) {
        try {
            return await request(options).auth(keys.apiLogin, keys.apiKey);
        } catch (error) {
            throw new BadRequestException(error.message);
        }
    }
}
