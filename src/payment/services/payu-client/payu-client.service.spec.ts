import { Test, TestingModule } from '@nestjs/testing';
import { PayuClientService } from './payu-client.service';

describe('PayuClientService', () => {
  let service: PayuClientService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PayuClientService],
    }).compile();

    service = module.get<PayuClientService>(PayuClientService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
