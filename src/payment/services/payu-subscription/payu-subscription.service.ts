import { Injectable, HttpService, BadRequestException } from '@nestjs/common';
import { database } from 'firebase-admin';
import { PayuKeys } from '../../models/payuKeys';
import * as request from 'request-promise';
import { CreateSubscription } from '../../dto/createSubscription.dto';

@Injectable()
export class PayuSubscriptionService {
    // ========================================================================
    // Variables
    // ========================================================================
    private readonly url = process.env.PAYU + '/rest/v4.9/subscriptions';

    private readonly payuRef = database().ref('payu');

    // ========================================================================
    // Constructor
    // ========================================================================

    constructor(private http: HttpService) {}

    // ========================================================================
    // Methods
    // ========================================================================

    async create(uid: string, createSubscription: CreateSubscription) {
        const keys = await this.getKeys(uid);
        const body = {
            quantity: '1',
            installments: '1',
            trialDays: '10',
            customer: {
                id: createSubscription.idUser,
                creditCards: [
                    {
                        token: createSubscription.tokenCard,
                    },
                ],
            },
            plan: {
                planCode: createSubscription.planCode,
            },
        };
        const options = {
            method: 'POST',
            url: this.url,
            body,
            json: true,
        };
        try {
            return await request(options).auth(keys.apiLogin, keys.apiKey);
        } catch (error) {
            throw new BadRequestException(error.message);
        }
    }

    // ========================================================================
    // Private Methods
    // ========================================================================
    private async getKeys(uid: string) {
        const snap = await this.payuRef.child(uid).once('value');
        return snap.val() as PayuKeys;
    }
}
