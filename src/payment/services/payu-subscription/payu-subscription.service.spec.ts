import { Test, TestingModule } from '@nestjs/testing';
import { PayuSubscriptionService } from './payu-subscription.service';

describe('PayuSubscriptionService', () => {
  let service: PayuSubscriptionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PayuSubscriptionService],
    }).compile();

    service = module.get<PayuSubscriptionService>(PayuSubscriptionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
