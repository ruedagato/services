import { Injectable, HttpService, BadRequestException } from '@nestjs/common';
import * as request from 'request-promise';
import { CreatePlanDto } from '../../dto/createPlan.dto';
import { database } from 'firebase-admin';
import { PayuKeys } from '../../models/payuKeys';

@Injectable()
export class PayuPlanService {
    // ========================================================================
    // Variables
    // ========================================================================
    private readonly url = process.env.PAYU + '/rest/v4.9/plans';

    private payuRef = database().ref('payu');

    // ========================================================================
    // Methods
    // ========================================================================

    async create(uid: string, planDto: CreatePlanDto) {
        const keys = await this.getKeys(uid);
        const plan = {
            accountId: keys.accountId,
            planCode: planDto.planCode,
            description: planDto.description,
            interval: planDto.interval,
            intervalCount: '1',
            maxPaymentsAllowed: '12',
            paymentAttemptsDelay: '1',
            additionalValues: [
                {
                    name: 'PLAN_VALUE',
                    value: planDto.value,
                    currency: planDto.currency,
                },
            ],
        };
        const options = {
            method: 'POST',
            url: this.url,
            body: plan,
            json: true,
        };
        try {
            return await request(options).auth(keys.apiLogin, keys.apiKey);
        } catch (error) {
            throw new BadRequestException(error.message);
        }
    }

    // ========================================================================
    // Private methods
    // ========================================================================
    async getKeys(uid: string) {
        const snap = await this.payuRef.child(uid).once('value');
        return snap.val() as PayuKeys;
    }
}
