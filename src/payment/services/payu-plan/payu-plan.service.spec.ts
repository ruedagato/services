import { Test, TestingModule } from '@nestjs/testing';
import { PayuPlanService } from './payu-plan.service';

describe('PayuPlanService', () => {
  let service: PayuPlanService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PayuPlanService],
    }).compile();

    service = module.get<PayuPlanService>(PayuPlanService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
