import { Base } from '../../utils/base';
import { IsString, IsNotEmpty } from 'class-validator';
import { Expose } from 'class-transformer';

export class PayuKeys extends Base {
    @IsString()
    @IsNotEmpty()
    @Expose()
    apiLogin: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    apiKey: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    country: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    currency: string;

    @IsString()
    @IsNotEmpty()
    @Expose()
    accountId: string;
}
