import { Module, HttpModule } from '@nestjs/common';
import { PayuController } from './controllers/payu/payu.controller';
import { PayuService } from './services/payu/payu.service';
import { PayuSubscriptionService } from './services/payu-subscription/payu-subscription.service';
import { PayuPlanService } from './services/payu-plan/payu-plan.service';
import { PayuClientService } from './services/payu-client/payu-client.service';

@Module({
  imports: [HttpModule],
  controllers: [PayuController],
  providers: [PayuService, PayuSubscriptionService, PayuPlanService, PayuClientService]
})
export class PaymentModule {}
