import { classToPlain } from 'class-transformer';

export class Base {
    serialize() {
        return classToPlain(this);
    }
}
