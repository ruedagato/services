import { createParamDecorator } from '@nestjs/common';
import * as admin from 'firebase-admin';

export const Company = createParamDecorator(async (data, req) => {
    const { headers } = req;
    const token: string = headers.authorization;
    const payload = Buffer.from(token.split(' ')[1], 'base64')
        .toString('ascii')
        .split(':');
    const user = await admin.auth().getUser(payload[0]);
    return { ...user };
});
