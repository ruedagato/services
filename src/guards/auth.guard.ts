import { CanActivate, ExecutionContext, Injectable, ForbiddenException } from '@nestjs/common';
import { Observable } from 'rxjs';
import * as admin from 'firebase-admin';
import * as cripto from 'crypto';

@Injectable()
export class AuthGuard implements CanActivate {
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const { headers } = context.switchToHttp().getRequest();
        const token: string = headers.authorization;
        const data = Buffer.from(token.split(' ')[1], 'base64')
            .toString('ascii')
            .split(':');
        const hash = this.generateHash(data[1]);

        return admin
            .database()
            .ref('user')
            .child(data[0])
            .once('value')
            .then(snap => snap.val() === hash);
    }

    private generateHash(value: string) {
        return cripto
            .createHmac('sha256', process.env.SECRETE)
            .update(value)
            .digest('hex');
    }
}
