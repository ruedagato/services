import { Injectable, BadRequestException, InternalServerErrorException } from '@nestjs/common';
import * as admin from 'firebase-admin';
import * as cripto from 'crypto';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class UserService {
    /**
     * Create a user in services
     * @param company Name of company
     * @param password Company password
     */
    async createUser(company: string, password: string) {
        const hash = this.generateHash(password);
        try {
            const user = await admin.auth().createUser({
                displayName: company,
                email: `${company}@services.com`,
                password,
            });
            const { uid } = user;
            admin
                .database()
                .ref('user')
                .child(uid)
                .set(hash);
            return user;
        } catch (error) {
            if (error.code === 'auth/email-already-exists') {
                throw new BadRequestException(`there is a company ${company} already`);
            } else {
                throw new InternalServerErrorException(error.message, error.code);
            }
        }
    }

    /**
     * Get token session
     * @param company Name of company
     * @param password Password of company
     */
    async login(company: string, password: string) {
        const user = await admin.auth().getUserByEmail(`${company}@services.com`);
        const hash = this.generateHash(password);
        if (user) {
            const userSnap = await admin
                .database()
                .ref('user')
                .child(user.uid)
                .once('value');
            if (userSnap.val() === hash) {
                const { uid, customClaims } = user;
                const token = this.signInJwt({ uid, customClaims });
                return { token };
            } else {
                throw new BadRequestException('company or password incorrect');
            }
        }
        throw new BadRequestException('company or password incorrect');
    }

    verifyToken(token: string) {
        return jwt.verify(token, process.env.JWT_SECRETE);
    }

    private signInJwt(payload: any) {
        return jwt.sign(payload, process.env.JWT_SECRETE, { expiresIn: '1h' });
    }

    private generateHash(value: string) {
        return cripto
            .createHmac('sha256', process.env.SECRETE)
            .update(value)
            .digest('hex');
    }
}
