import { Controller, Post, Body, Get, Headers } from '@nestjs/common';
import { UserService } from '../../services/user/user.service';
import { TokenExpiredError } from 'jsonwebtoken';

@Controller('user')
export class UserController {
    constructor(private userService: UserService) {}

    @Post()
    createUser(@Body() data: { company: string; password: string }) {
        return this.userService.createUser(data.company, data.password);
    }

    @Post('login')
    login(@Body() data: { company: string; password: string }) {
        return this.userService.login(data.company, data.password);
    }

}
